Dear Maintainer,

Please find attached the French translation, proofread by the
debian-l10n-french mailing list contributors.

This file should be put as fr.po in the appropriate place in
your package build tree.

Best regards,

Alban Vidal
