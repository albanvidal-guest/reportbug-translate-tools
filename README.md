Reportbug translate tools
=========================

Tools for send easily a reportbug to mainteners

--------------------------------------------------------------------------------

## Setup and configuration

### Step 1 - Clone this repository

By example, in `/srv/git/` directory.

```bash
mkdir /srv/git
git clone https://salsa.debian.org/albanvidal-guest/reportbug-translate-tools.git /srv/git/
cd /srv/git/reportbug-translate-tools/
```

### Step 2 - Create a symbolic lynk of script in `/usr/local/bin` directory

```
sudo ln -s /srv/git/reportbug-translate-tools/reportbug-translate-tools /usr/local/bin/
```

### Step 3 - Copy the content of `template` directory to your home directory.

You need to create configuration file and translation template text.

```bash
mkdir $HOME/.reportbug-translate-tools/
cp ./template/* $HOME/.reportbug-translate-tools/
```

### Step 4 - Edit the value of files you just copied

```bash
# Edit with vim, nano or as you want
vi $HOME/.reportbug-translate-tools/*
```

--------------------------------------------------------------------------------

### Usage example - Translation is ready to send

+ You need to have `fr.po` file, remplace **fr** by your l10n code.
+ You need to have your translate file validate by your l10n team.
+ Move to your translate directory, by example: `cd /srv/debian-translation/debconf/nova`
+ Execute the following command to send your reportbug :

```bash
# Usage
reportbug-translate-tools [Package_name] [Pagnage_version]

# Example:
reportbug-translate-tools nova 2_18.0.3-4
# select the translate type
# confirm the reportbug send
```
